# README #

This is a simple text-to-keypad-sequence converter.
The Latin alphabet contains 26 characters and telephones only have ten digits on the keypad.
Thus for example to type 'a' you need to press button '2',
to type 'b' you need to press '2' two times, etc.

In order to insert two characters in sequence from the same key,
the user must pause before pressing the key a second time.
The space character is printed to indicate a pause.
For example, "2 2" indicates "aa" whereas "22" indicates "b".

### Building ###

* Clone repo
* Build

```bash
$ mkdir build
$ cd build
$ cmake .. # use -DCMAKE_CXX_FLAGS=-DKPDEBUG to enable tracing
$ make
```

Build was tested with the following compilers:

* gcc 9.3
* gcc 6.5
* clang 9.0

### Running and configuring ###

The program accepts one optional argument - path to configuration file.
If argument is omitted the default one (./latin.kp) is used.
If argument is "--help" usage string is displayed.

Configuration file contains map of characters to buttons sequence.
Format:

```
<char><space_or_tab><sequence_of_buttons>
```

Empty line are skipped.

Example:

```
a 2
b 22

c 222
d 3
```

The table for latin keyboard is in **conf/latin.kp** file.

The program reads from stdin and writes to stdout.
Input is in following format: first line contains number of cases (**N**),
further lines contain input strings.

Example:

```
2
hi
hello world
```

Lines after **N** are ignored.

Example:

```bash
$ ./bin/kpspelling 
2
hi
Case #1: 44 444
hello world
Case #2: 4433555 555666096667775553
```

### Testing ###
After successfull build the contents of **test** directory is copied to build directory.
Testing is performed by perl **Test::More** module.
Test-cases are located in **t/\*.t** files.
Test inputs and configs are in **t/input** and **t/config** subdirs.

To launch testing run the following command:

```bash
$ prove
```

Example:

```
$ prove
t/cmd.t ..... ok   
t/config.t .. ok   
t/input.t ... ok    
All tests successful.
Files=3, Tests=23,  0 wallclock secs ( 0.03 usr  0.02 sys +  0.31 cusr  0.07 csys =  0.43 CPU)
Result: PASS
```