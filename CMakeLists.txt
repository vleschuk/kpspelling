cmake_minimum_required(VERSION 2.8)

set(PROJECT keypad_spelling)
project(${PROJECT})

set(CMAKE_CXX_STANDARD 11)
include_directories(${CMAKE_CURRENT_SOURCE_DIR}/include)
add_subdirectory(src)
