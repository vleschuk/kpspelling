#ifndef KP_DEBUG_H_
#define KP_DEBUG_H_

#ifdef KPDEBUG
#define KP_DEBUG(msg) do \
  std::cerr << msg << std::endl; \
while (0)
#else
#define KP_DEBUG(msg)
#endif

#endif // KP_DEBUG_H_
