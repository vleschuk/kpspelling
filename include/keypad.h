#ifndef KP_KEYPAD_H_
#define KP_KEYPAD_H_

#include <unordered_map>
#include <string>

namespace kpspelling {

class KeyPad {
  typedef std::unordered_map<char, std::string> Table;

public:
  static const std::string DEFAULT_KEYPAD_FILE;
  static const std::string PAUSE;

public:
  KeyPad(const std::string &keyPadFile);
  ~KeyPad() = default;
  KeyPad(const KeyPad &) = delete;
  KeyPad &operator=(const KeyPad &) = delete;

  std::string getButtons(char c) const;
  std::string getButtons(const std::string &str) const;

private:
  void populateTable();
  bool parseConfigLine(const std::string &line);

private:
  const std::string conf_;
  Table table_;
};

} // namespace kpspelling
#endif // KP_KEYPAD_H_
