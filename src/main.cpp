#include <cstring>
#include <iostream>
#include <sstream>
#include <stdexcept>

#include "debug.h"
#include "keypad.h"

namespace {

struct Config {
  bool showHelp = false;
  std::string kpFile = kpspelling::KeyPad::DEFAULT_KEYPAD_FILE;
};

// TODO: proper cmd args parsing
Config ParseCmd(int argc, char **argv) {
  static const char helpStr[] = "--help";

  Config conf;
  if (argc == 1) {
    return conf;
  }

  if (argc > 2 || !strncmp(argv[1], helpStr, sizeof(helpStr) - 1)) {
    conf.showHelp = true;
    return conf;
  }

  conf.kpFile = std::string(argv[1]);
  return conf;
}

void ProcessInput(const kpspelling::KeyPad &kp) {
  std::string input;

  if (!std::getline(std::cin, input)) {
    throw std::runtime_error("Failed to get number of input lines");
  }

  if (input.empty()) {
    throw std::runtime_error("Empty input");
  }

  for (auto c : input) {
    if (!std::isdigit(c)) {
      throw std::runtime_error("Non-numeric input when reading number of cases");
    }
  }

  unsigned n = static_cast<unsigned>(atoi(input.c_str()));
  KP_DEBUG("Will process " << n << " lines");
  for(size_t i = 1; i <= n; ++i) {
    if (!std::getline(std::cin, input)) {
      if (std::cin.eof()) {
        std::ostringstream ss;
        ss << "EOF before line #" << i << " of " << n;
        throw std::runtime_error(ss.str().c_str());
      }
      throw std::runtime_error("Failed to get input line");
    }
    auto s = kp.getButtons(input);
    std::cout << "Case #" << i << ": " << s << std::endl;
  }
}

} // anonymous namespace

int main(int argc, char **argv) {
  try {
    Config conf(ParseCmd(argc, argv));
    if (conf.showHelp) {
      std::cout << "Usage: " << argv[0] << " <path_to_config>" << std::endl;
      return 0;
    }
    kpspelling::KeyPad kp(conf.kpFile);
    ProcessInput(kp);
  }
  catch (const std::exception &e) {
    std::cout << "Error: " << e.what() << std::endl;
    return 1;
  }
  catch (...) {
    std::cout << "Unknown error" << std::endl;
    return 1;
  }

  return 0;
}
