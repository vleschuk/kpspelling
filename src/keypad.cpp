#include <cassert>
#include <cctype>
#include <fstream>
#include <iostream>
#include <sstream>
#include <stdexcept>

#include "debug.h"
#include "keypad.h"

namespace {

bool ButtonsValid(const std::string &s) {
  for (auto c : s) {
    if (!std::isdigit(c)) {
      return false;
    }
  }
  return true;
}

} // anonymous namespace

namespace kpspelling {

const std::string KeyPad::DEFAULT_KEYPAD_FILE = "./latin.kp";
const std::string KeyPad::PAUSE = " ";

KeyPad::KeyPad(const std::string &keyPadFile)
  : conf_(keyPadFile) {
  populateTable();
}

void KeyPad::populateTable() {
  std::ifstream input(conf_);

  if (!input.good()) {
    std::ostringstream ss;
    ss << "Failed to open config file '" << conf_ << "' for reading";
    throw std::runtime_error(ss.str().c_str());
  }

  std::string line;
  while (std::getline(input, line)) {
    if (line.empty()) {
      KP_DEBUG("Empty line");
      continue;
    }
    if (!parseConfigLine(line)) {
      std::ostringstream ss;
      ss << "Failed to parse config line: '" << line << "'" << std::endl;
      throw std::runtime_error(ss.str().c_str());
    }
    KP_DEBUG("Successfully parsed line: '" << line << "'");
  }

  if (table_.empty()) {
    std::ostringstream ss;
    ss << "Empty config file '" << conf_ << "'";
    throw std::runtime_error(ss.str().c_str());
  }
}

bool KeyPad::parseConfigLine(const std::string &line) {
  assert(!line.empty() && "Line is not empty");

  if (line.size() < 3 || !std::isspace(line[1])) {
    std::ostringstream ss;
    ss << "Invalid format. Line: '" << line << "'";
    throw std::runtime_error(ss.str().c_str());
  }

  char c = line[0];
  std::string buttons = line.substr(2); 
  KP_DEBUG("char: '" << c << "' buttons: '" << buttons << "'");

  if (table_.count(c)) {
    std::ostringstream ss;
    ss << "Duplicate entry '" << c << "' on  config line: '"
       << line << "'" << std::endl;
    throw std::runtime_error(ss.str().c_str());
  }

  if (!ButtonsValid(buttons)) {
    std::ostringstream ss;
    ss << "Invalid buttons set: '" << buttons << "'";
    throw std::runtime_error(ss.str().c_str());
  }

  table_[c] = buttons;

  return true;
}

std::string KeyPad::getButtons(char c) const {
  const auto it = table_.find(c);
  if (it == table_.end()) {
    KP_DEBUG("Character '" << c << "' not found");
    return std::string(); 
  }
  return it->second;
}

std::string KeyPad::getButtons(const std::string &str) const {
  std::string res;
  for(auto c : str) {
    auto buttons = getButtons(c);
    if (buttons.empty()) {
      std::ostringstream ss;
      ss << "Can't get buttons sequence for character '" << c << "'" << std::endl;
      throw std::runtime_error(ss.str().c_str());
    }
    if (!res.empty() && res[res.size() - 1] == buttons[0]) {
      res.append(PAUSE);
    }
    res.append(buttons);
  }
  return res;
}

} // namespace kpspelling
