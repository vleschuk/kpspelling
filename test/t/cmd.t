use FindBin qw/ $Bin /;
use Test::More qw/ no_plan /;

use strict;
use warnings;

use constant DEFAULT_CONFIG => "latin.kp";

my $prog = $ENV{KPBIN} || "$Bin/../bin/kpspelling";
my $confdir = "$Bin/config";
my $inputdir = "$Bin/input";

{
  open my $fh, "$prog --help |" or die "Can't open $prog";
  my $output = <$fh>;
  ok($output =~ m/^Usage:/, "--help option works");
  close $fh;
}

{
  open my $fh, "$prog 1 2 3 |" or die "Can't open $prog";
  my $output = <$fh>;
  ok($output =~ m/^Usage:/, "More than 1 argument leads to usage display");
  close $fh;
}

if (-f DEFAULT_CONFIG) {
  my $res = system("$prog < $inputdir/zero");
  ok($res == 0, "Default config is used with no arguments");
}

{
  my $res = system("$prog $confdir/valid.kp < $inputdir/zero");
  ok($res == 0, "Provided config is used when only 1 arg is passed");
}
