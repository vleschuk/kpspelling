use FindBin qw/ $Bin /;
use Test::More qw/ no_plan /;

use strict;
use warnings;

use constant DEFAULT_CONFIG => "latin.kp";

my $prog = $ENV{KPBIN} || "$Bin/../bin/kpspelling";
my $confdir = "$Bin/config";
my $inputdir = "$Bin/input";
my $cmd = "$prog $confdir/valid.kp";

{
  my $in = "$inputdir/empty";
  open my $fh, "$cmd < $in |" or die "Can't open $prog";
  my $output = <$fh>;
  ok($output =~ m/^Error: Failed to get number/i, "Error on empty input");
  close $fh;
}

{
  my $in = "$inputdir/zero";
  open my $fh, "$cmd < $in |" or die "Can't open $prog";
  my $output = <$fh>;
  ok(!length($output), "No output and no error on n == 0");
  close $fh;
}

{
  my $in = "$inputdir/nopause";
  open my $fh, "$cmd < $in |" or die "Can't open $prog";
  my $output = <$fh>;
  chomp $output;
  ok($output eq "Case #1: 23", "Valid output on single line with no pause");
  close $fh;
}

{
  my $in = "$inputdir/pause";
  open my $fh, "$cmd < $in |" or die "Can't open $prog";
  my $output = <$fh>;
  chomp $output;
  ok($output eq "Case #1: 2 22", "Valid output on single line with pause");
  close $fh;
}

{
  my $in = "$inputdir/space";
  open my $fh, "$cmd < $in |" or die "Can't open $prog";
  my $output = <$fh>;
  chomp $output;
  ok($output eq "Case #1: 203", "Valid output on single line with space");
  close $fh;
}

{
  my $in = "$inputdir/twolines";
  open my $fh, "$cmd < $in |" or die "Can't open $prog";
  my $output = <$fh>;
  chomp $output;
  ok($output eq "Case #1: 2", "Valid output on first line of two");
  $output = <$fh>;
  chomp $output;
  ok($output eq "Case #2: 22", "Valid output on second line of two");
  close $fh;
}

{
  my $in = "$inputdir/short";
  open my $fh, "$cmd < $in |" or die "Can't open $prog";
  my $output = <$fh>;
  chomp $output;
  ok($output eq "Case #1: 2", "Valid output on first line of two");
  $output = <$fh>;
  ok($output =~ m/^Error: EOF before line #2 of 2/i, "Error on early EOF");
  close $fh;
}

{
  my $in = "$inputdir/extraline";
  open my $fh, "$cmd < $in |" or die "Can't open $prog";
  my $output = <$fh>;
  chomp $output;
  ok($output eq "Case #1: 2", "Valid output on first line of two");
  $output = <$fh>;
  chomp $output;
  ok($output eq "Case #2: 22", "Valid output on second line of two");
  $output = <$fh>;
  ok(!length($output), "No output and no error on extra line 3 with n == 2");
  close $fh;
}

{
  my $in = "$inputdir/invalid";
  open my $fh, "$cmd < $in |" or die "Can't open $prog";
  my $output = <$fh>;
  ok($output =~ m/^Error: Can't get buttons/i, "Error on invalid input");
  close $fh;
}
