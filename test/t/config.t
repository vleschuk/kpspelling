use FindBin qw/ $Bin /;
use Test::More qw/ no_plan /;

use strict;
use warnings;

use constant DEFAULT_CONFIG => "latin.kp";

my $prog = $ENV{KPBIN} || "$Bin/../bin/kpspelling";
my $confdir = "$Bin/config";
my $inputdir = "$Bin/input";

{
  my $conf = "$confdir/valid.kp";
  my $res = system("$prog $conf < $inputdir/zero");
  ok($res == 0, "Valid config parsed");
}

{
  my $conf = "$confdir/empty.kp";
  open my $fh, "$prog $conf < $inputdir/zero |" or die "Can't open $prog";
  my $output = <$fh>;
  ok($output =~ m/^Error: empty config/i, "Error on empty config");
  close $fh;
}

{
  my $conf = "$confdir/empty_line.kp";
  my $res = system("$prog $conf < $inputdir/zero");
  ok($res == 0, "Config with empty line parsed");
}

{
  my $conf = "$confdir/invalid_numbers.kp";
  open my $fh, "$prog $conf < $inputdir/zero |" or die "Can't open $prog";
  my $output = <$fh>;
  ok($output =~ m/^Error: Invalid buttons/i, "Error on invalid numbers config");
  close $fh;
}

{
  my $conf = "$confdir/short_line.kp";
  open my $fh, "$prog $conf < $inputdir/zero |" or die "Can't open $prog";
  my $output = <$fh>;
  ok($output =~ m/^Error: Invalid format/i, "Error on short line config");
  close $fh;
}

{
  my $conf = "$confdir/duplicate.kp";
  open my $fh, "$prog $conf < $inputdir/zero |" or die "Can't open $prog";
  my $output = <$fh>;
  ok($output =~ m/^Error: Duplicate entry/i, "Error on duplicate entry config");
  close $fh;
}
